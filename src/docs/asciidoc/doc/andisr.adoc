==== ANDI to SR
'''

*NAME*

    ANDI to SR -- Logical AND Immediate to SR (privileged)

*SYNOPSIS*
....
    ANDI    #<data>,SR

    Size = (Word)

....
*FUNCTION*
....
    Performs a bit-wise AND operation with the immediate data and
    the status register. All implemented bits of the status register are
    affected.

....
*FORMAT*
....
    -----------------------------------------------------------------
    |15 |14 |13 |12 |11 |10 | 9 | 8 | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
    |---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
    | 0 | 0 | 0 | 0 | 0 | 0 | 1 | 0 | 0 | 1 | 1 | 1 | 1 | 1 | 0 | 0 |
    |---------------------------------------------------------------|
    |                     16 BITS IMMEDIATE DATA                    |
    -----------------------------------------------------------------

....
*RESULT*
....
    X - Cleared if bit 4 of immed. operand is zero. Unchanged otherwise.
    N - Cleared if bit 3 of immed. operand is zero. Unchanged otherwise.
    Z - Cleared if bit 2 of immed. operand is zero. Unchanged otherwise.
    V - Cleared if bit 1 of immed. operand is zero. Unchanged otherwise.
    C - Cleared if bit 0 of immed. operand is zero. Unchanged otherwise.

....
'''
[%hardbreaks]
SEE ALSO
    <<AND>> <<ANDI>> <<ANDI to CCR>>
        

