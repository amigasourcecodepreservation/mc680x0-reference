==== CAS
'''

*NAME*

    CAS -- Compare And Swap

*SYNOPSIS*
....
    CAS Dc,Du,<ea>

    Size = (Byte, Word, Long)

....
*FUNCTION*
....
    This instruction is a read-modify-write instruction and should
    NEVER be used on Amiga because of conflicts with customs chips.
    Destination operand, which is in memory at address specified
    by <ea>, is compared to  data register Dc (Data Compare). This
    register is used as reference in the principle of this instruction.
    If there's equality (Z=1), destination operand can be updated, i.e.
    the new operand Du (Data Update) is moved in destination.
    If there's no equality (Z=0), it's the reference register Dc which
    must be updated. So there's a move from destination operand to Dc.

....
*FORMAT*
....
                                                          <ea>
    ----------------------------------------=========================
    |15 |14 |13 |12 |11 |10 | 9 | 8 | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
    |---|---|---|---|---|-------|---|---|---|-----------|-----------|
    | 0 | 0 | 0 | 0 | 1 | SIZE  | 0 | 1 | 1 |   MODE    |  REGISTER |
    |---|---|---|---|---|-------|-----------|-----------|-----------|
    | 0 | 0 | 0 | 0 | 0 | 0 | 0 |Du REGISTER| 0 | 0 | 0 |Dc REGISTER|
    -----------------------------------------------------------------

SIZE
    01->one Byte operation
    10->one Word operation
    11->one Long operation

....
*REGISTER*
....
    Du register: indicates number of data register, which contains the
    new value to update in destination operand.

    Dc register: indicates number of data register, which contains the
    reference value to compare to destination operand.

    <ea> is always destination, addressing modes are the followings:
    --------------------------------- -------------------------------
    |Addressing Mode|Mode| Register | |Addressing Mode|Mode|Register|
    |-------------------------------| |-----------------------------|
    |      Dn       | -  |     -    | |    Abs.W      |111 |  000   |
    |-------------------------------| |-----------------------------|
    |      An       | -  |     -    | |    Abs.L      |111 |  001   |
    |-------------------------------| |-----------------------------|
    |     (An)      |010 |N° reg. An| |   (d16,PC)    | -  |   -    |
    |-------------------------------| |-----------------------------|
    |     (An)+     |011 |N° reg. An| |   (d8,PC,Xi)  | -  |   -    |
    |-------------------------------| |-----------------------------|
    |    -(An)      |100 |N° reg. An| |   (bd,PC,Xi)  | -  |   -    |
    |-------------------------------| |-----------------------------|
    |    (d16,An)   |101 |N° reg. An| |([bd,PC,Xi],od)| -  |   -    |
    |-------------------------------| |-----------------------------|
    |   (d8,An,Xi)  |110 |N° reg. An| |([bd,PC],Xi,od)| -  |   -    |
    |-------------------------------| |-----------------------------|
    |   (bd,An,Xi)  |110 |N° reg. An| |    #data      | -  |   -    |
    |-------------------------------| -------------------------------
    |([bd,An,Xi]od) |110 |N° reg. An|
    |-------------------------------|
    |([bd,An],Xi,od)|110 |N° reg. An|
    ---------------------------------

....
*RESULT*
....
    X - not affected
    N - Set if the result of comparison is negative. Cleared otherwise.
    Z - Set if the result of comparison is zero. Cleared otherwise.
    V - Set if overflow. Cleared otherwise.
    C - Set if carry. Cleared otherwise.

....
'''
[%hardbreaks]
SEE ALSO
    <<CAS2>>
        

