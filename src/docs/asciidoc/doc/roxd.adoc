==== ROXL, ROXD
'''

*NAME*

    ROXL, ROXD -- ROtate Left with eXtend and ROtate Right with eXtend

*SYNOPSIS*
....
    ROXd    Dx,Dy
    ROXd    #<data>,Dy
    ROXd    <ea>
    where d is direction, L or R

    Size = (Byte, Word, Long)

....
*FUNCTION*
....
        A rotation is made on destination operand bits.
        Rotation uses bit X.

                  .-------->-----------.
    ROXL:     |                    |
          C <------ OPERAND <- X --'
                      <---

    ROXR:    ,---------<-------.
             |                 |
             `-- X -> OPERAND -----> C
                       --->

....
*FORMAT*
....
    In the case of the rotating of a register:
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -----------------------------------------------------------------
    |15 |14 |13 |12 |11 |10 | 9 | 8 | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
    |---|---|---|---|-----------|---|-------|---|---|---|-----------|
    | 1 | 1 | 1 | 0 |  NUMBER/  |dr |  SIZE |i/r| 1 | 0 | REGISTER  |
    |   |   |   |   |  REGISTER |   |       |   |   |   |           |
    -----------------------------------------------------------------

    In the case of the rotating of a memory area:
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -----------------------------------------------------------------
    |15 |14 |13 |12 |11 |10 | 9 | 8 | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
    |---|---|---|---|---|---|---|---|---|---|-----------|-----------|
    | 1 | 1 | 1 | 0 | 0 | 1 | 0 |dr | 1 | 1 |    MODE   | REGISTER  |
    ----------------------------------------=========================
                                                  <ea>

NUMBER/REGISTER
    Specifies number of rotating or number of register which contents
    the number of rotating.
    If i/r = 0, number of rotating is specified in the instruction as
    immediate data
    If i/r = 1, it's specified in the data register.
    If dr = 0, right rotating
    If dr = 1, left rotating

SIZE
    00->one Byte operation
    01->one Word operation
    10->one Long operation

....
*REGISTER*
....
    For a register rotating:
    Indicates the number of data register on which rotating is applied.

    For a memory rotating:
    <ea> indicates operand which should be rotated.
    Only addressing modes relatives to memory are allowed:

    --------------------------------- -------------------------------
    |Addressing Mode|Mode| Register | |Addressing Mode|Mode|Register|
    |-------------------------------| |-----------------------------|
    |      Dn       | -  |     -    | |    Abs.W      |111 |  000   |
    |-------------------------------| |-----------------------------|
    |      An       | -  |     -    | |    Abs.L      |111 |  001   |
    |-------------------------------| |-----------------------------|
    |     (An)      |010 |N° reg. An| |   (d16,PC)    | -  |   -    |
    |-------------------------------| |-----------------------------|
    |     (An)+     |011 |N° reg. An| |   (d8,PC,Xi)  | -  |   -    |
    |-------------------------------| |-----------------------------|
    |    -(An)      |100 |N° reg. An| |   (bd,PC,Xi)  | -  |   -    |
    |-------------------------------| |-----------------------------|
    |    (d16,An)   |101 |N° reg. An| |([bd,PC,Xi],od)| -  |   -    |
    |-------------------------------| |-----------------------------|
    |   (d8,An,Xi)  |110 |N° reg. An| |([bd,PC],Xi,od)| -  |   -    |
    |-------------------------------| |-----------------------------|
    |   (bd,An,Xi)  |110 |N° reg. An| |    #data      | -  |   -    |
    |-------------------------------| -------------------------------
    |([bd,An,Xi]od) |110 |N° reg. An|
    |-------------------------------|
    |([bd,An],Xi,od)|110 |N° reg. An|
    ---------------------------------

....
*RESULT*
....
    X - Set by the last bit out of operand.
            Not changed if rotation is zero.
    N - Set if the result is negative. Cleared otherwise.
    Z - Set if the result is zero. Cleared otherwise.
    V - Always cleared
    C - Set according to the last bit shifted out of the operand.

....
'''
[%hardbreaks]
SEE ALSO
    <<ROL, ROR>>    <<ASL, ASR>>    <<LSL, LSR>>
        

