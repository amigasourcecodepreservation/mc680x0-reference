==== TAS
'''

*NAME*

    TAS -- Test And Set operand

*SYNOPSIS*
....
    TAS <ea>

    Size = (Byte)

....
*FUNCTION*
....
    Test of a byte addressed by <ea>, bits N and Z of SR are set
    according to result of test.
    Bit 7 of byte is set to 1. This instruction uses read-modify-write
    cycle, which is not dividable and allows synchronisation of several
    processors. But this instruction is NOT ALLOWED ON AMIGA!
    This instruction can easily be substituted by BSET.

....
*FORMAT*
....
    -----------------------------------------------------------------
    |15 |14 |13 |12 |11 |10 | 9 | 8 | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
    |---|---|---|---|---|---|---|---|---|---|-----------|-----------|
    | 0 | 1 | 0 | 0 | 1 | 0 | 1 | 0 | 1 | 1 |    MODE   | REGISTER  |
    ----------------------------------------=========================
                                                   <ea>

....
*REGISTER*
....
    <ea> is destination, addressing modes are the followings:
    --------------------------------- -------------------------------
    |Addressing Mode|Mode| Register | |Addressing Mode|Mode|Register|
    |-------------------------------| |-----------------------------|
    |      Dn       |000 |N° reg. Dn| |    Abs.W      |111 |  000   |
    |-------------------------------| |-----------------------------|
    |      An       | -  |     -    | |    Abs.L      |111 |  001   |
    |-------------------------------| |-----------------------------|
    |     (An)      |010 |N° reg. An| |   (d16,PC)    | -  |   -    |
    |-------------------------------| |-----------------------------|
    |     (An)+     |011 |N° reg. An| |   (d8,PC,Xi)  | -  |   -    |
    |-------------------------------| |-----------------------------|
    |    -(An)      |100 |N° reg. An| |   (bd,PC,Xi)  | -  |   -    |
    |-------------------------------| |-----------------------------|
    |    (d16,An)   |101 |N° reg. An| |([bd,PC,Xi],od)| -  |   -    |
    |-------------------------------| |-----------------------------|
    |   (d8,An,Xi)  |110 |N° reg. An| |([bd,PC],Xi,od)| -  |   -    |
    |-------------------------------| |-----------------------------|
    |   (bd,An,Xi)  |110 |N° reg. An| |    #data      | -  |   -    |
    |-------------------------------| -------------------------------
    |([bd,An,Xi]od) |110 |N° reg. An|
    |-------------------------------|
    |([bd,An],Xi,od)|110 |N° reg. An|
    ---------------------------------

....
*RESULT*
....
    X - Not affected.
    N - Set if MSB of byte is set. Cleared otherwise.
    Z - Set if byte is zero. Cleared otherwise.
    V - Always cleared.
    C - Always cleared.

....
'''
[%hardbreaks]
SEE ALSO

