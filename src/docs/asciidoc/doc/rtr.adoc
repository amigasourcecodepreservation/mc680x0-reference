==== RTR
'''

*NAME*

    RTR -- ReTurn and Restore CCR

*SYNOPSIS*
....
    RTR

....
*FUNCTION*
....
    CCR and PC are restored by SP.
    Supervisor byte of SR isn't affected.

....
*FORMAT*
....
    -----------------------------------------------------------------
    |15 |14 |13 |12 |11 |10 | 9 | 8 | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
    |---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
    | 0 | 1 | 0 | 0 | 1 | 1 | 1 | 0 | 0 | 1 | 1 | 1 | 0 | 1 | 1 | 1 |
    -----------------------------------------------------------------

....
*RESULT*
....
    SR is set following to the restored word taken from SP.

....
'''
[%hardbreaks]
SEE ALSO
    <<RTS>> <<RTE>> <<RTD>>
        

